import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null
  },
  getters: {
    user: state => {
      return state.user;
    }
  },
  mutations: {
    setUser(store, data) {
      store.user = data.user
    }
  },
  actions: {
    async login(store, data) {
      const login = await Axios.get(
        "http://localhost:3000/users?username=" +
        data.username +
        "&password=" +
        data.password
      );
      if (login.data.length === 1) {
        store.commit('setUser', { user: login.data[0] });
      }
      localStorage.setItem('user-id', login.data[0].id)
    },
    logout(store) {
      store.commit('setUser', { user: null });
      localStorage.clear()
    },
    async logFromLocalStorage(store) {
      const login = await Axios.get(
        "http://localhost:3000/users/" + localStorage.getItem('user-id')
      );
      store.commit('setUser', { user: login.data });
    }
  },
  modules: {
  }
})
