import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import { colors } from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: colors.red,
        secondary: colors.red.darken4,
        accent: colors.red.lighten5,
      }
    }
  }
});
